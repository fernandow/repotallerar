var clientesObtenidos;

function gestionClientes() {
    var url="https://services.odata.org/V4/Northwind/Northwind.svc/Customers";
    var request = new XMLHttpRequest();

    request.onreadystatechange = function(){

      if (this.readyState == 4 && this.status == 200){
        //console.table(JSON.parse(request.responseText).value);
        clientesObtenidos = request.responseText;

        procesarClientes();
      }
    }

    request.open("GET", url, true);
    request.send();
}

function procesarClientes() {

  var rutaBandera = "https://www.countries-ofthe-world.com/flags-normal/flag-of-";

  var JSONClientes = JSON.parse(clientesObtenidos);

  var divTabla = divTablaClientes;
  var tabla = document.createElement("table");
  var tbody = document.createElement("tbody");

  tabla.classList.add("table");
  tabla.classList.add("table-striped");

  for (var i=0; i< JSONClientes.value.length; i++){
      var nuevaFila = document.createElement("tr");

      var columnaNombreContacto = document.createElement("td");
      columnaNombreContacto.innerText = JSONClientes.value[i].ContactName;

      var columnaNombreCompania = document.createElement("td");
      columnaNombreCompania.innerText = JSONClientes.value[i].CompanyName;

      var columnaPais = document.createElement("td");
      var imgBandera = document.createElement("img");
      imgBandera.src = rutaBandera + JSONClientes.value[i].Country + ".png";
      imgBandera.classList.add("flag");
      columnaPais.appendChild(imgBandera);

      nuevaFila.appendChild(columnaNombreContacto);
      nuevaFila.appendChild(columnaNombreCompania);
      nuevaFila.appendChild(columnaPais);

      tbody.appendChild(nuevaFila);
  }

  tabla.appendChild(tbody);
  divTablaClientes.appendChild(tabla);
}
